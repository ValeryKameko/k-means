import Point from './Point';
import findMinimum from './helper';
import Group from './Group';

export default class KMeansProcessor {
  static process(groups) {
    const newGroups = KMeansProcessor.recalculateCentroids(groups);
    return KMeansProcessor.groupPoints(newGroups);
  }

  static groupPoints(groups) {
    const points = groups.flatMap((group) => group.points);
    const centroids = groups.map((group) => group.centroid);
    const pointGroups = points.map((point) => {
      const closestCentroid = findMinimum(centroids, (centroid) => Point.distance(centroid, point));
      return {
        point,
        closestCentroid,
      };
    });
    const newGroups = centroids.map((centroid) => {
      const groupPoints = pointGroups.filter(
        (pointGroup) => pointGroup.closestCentroid === centroid,
      );
      groupPoints.sort((a, b) => a - b);
      return new Group(centroid, groupPoints.map(((groupPoint) => groupPoint.point)));
    });
    return newGroups;
  }

  static recalculateCentroids(groups) {
    return groups.map(({ centroid, points }) => {
      const pointsSum = points.reduce(Point.sum, new Point(0, 0));
      const newCentroid = points.length === 0 ? centroid : pointsSum.divide(points.length);
      return new Group(newCentroid, points);
    });
  }

  static isFinished(groups, newGroups) {
    let isFinished = true;
    groups.forEach(({ points }) => {
      const newGroup = newGroups.find(
        (group) => KMeansProcessor.isPointsEqual(points, group.points),
      );
      if (!newGroup) isFinished = false;
    });
    return isFinished;
  }

  static isPointsEqual(points, newPoints) {
    let isEqual = true;
    points.forEach((point) => {
      const newPoint = newPoints.find((other) => other === point);
      if (!newPoint) isEqual = false;
    });
    return isEqual;
  }
}
