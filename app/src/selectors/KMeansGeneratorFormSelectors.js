import { formValueSelector } from 'redux-form';

export const generatorFormSelector = formValueSelector('generatorForm');

export const clustersSelector = (state) => Number.parseInt(generatorFormSelector(state, 'clusters'), 10);
export const pointsSelector = (state) => Number.parseInt(generatorFormSelector(state, 'points'), 10);
export const gaussianCountsSelector = (state) => Number.parseInt(generatorFormSelector(state, 'gaussianCounts'), 10);
export const gaussianSigmaSelector = (state) => Number.parseInt(generatorFormSelector(state, 'gaussianSigma'), 10);
export const generatorTypeSelector = (state) => generatorFormSelector(state, 'generatorType');
