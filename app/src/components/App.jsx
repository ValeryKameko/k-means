import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import {
  Container, Header, Grid,
} from 'semantic-ui-react';

import KMeansVisualizerContainer from '../containers/KMeansVisualizerContainer';
import KMeansStepperContainer from '../containers/KMeansStepperContainer';
import KMeansGeneratorContainer from '../containers/KMeansGeneratorContainer';
import { getIsGenerated } from '../selectors';

const defaultProps = {
};

const propTypes = {
  isGenerated: PropTypes.bool.isRequired,
};

const App = ({ isGenerated }) => (
  <Container fluid>
    <Header block size="huge">
      <Header.Content>K-means visualization application</Header.Content>
      <Header.Subheader>by Valerich</Header.Subheader>
    </Header>
    <Grid columns={2}>
      <div className="column" style={{ flex: '0 0 300px' }}>
        <KMeansVisualizerContainer
          width={600}
          height={600}
        />
      </div>
      <div className="column" style={{ flex: 1 }}>
        {isGenerated
          ? <KMeansStepperContainer />
          : <KMeansGeneratorContainer />}
      </div>
    </Grid>
  </Container>
);

App.defaultProps = defaultProps;
App.propTypes = propTypes;

const mapStateToProps = (state) => ({
  isGenerated: getIsGenerated(state),
});

export default connect(mapStateToProps)(App);
