import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, Container, Header, Segment,
} from 'semantic-ui-react';

const defaultProps = {
};

const propTypes = {
  step: PropTypes.func.isRequired,
  start: PropTypes.func.isRequired,
  pause: PropTypes.func.isRequired,
  restart: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  isFinished: PropTypes.bool.isRequired,
  stepCount: PropTypes.number.isRequired,
};

const KMeansStepper = ({
  start,
  pause,
  step,
  back,
  restart,
  isPlaying,
  isFinished,
  stepCount,
}) => (
  <Container fluid>
    <Header as="h2">Control</Header>
    <Segment>
      Steps:
      {stepCount}
    </Segment>
    <Button
      content="Next"
      icon="right arrow"
      labelPosition="right"
      onClick={step}
      disabled={isFinished}
    />
    <Button
      content="Start"
      icon="play"
      labelPosition="right"
      onClick={start}
      disabled={isPlaying || isFinished}
    />
    <Button
      content="Pause"
      icon="pause"
      labelPosition="right"
      onClick={pause}
      disabled={!isPlaying || isFinished}
    />
    <Button
      content="Restart"
      icon="sync"
      labelPosition="right"
      onClick={restart}
    />
    <Button
      content="Back"
      icon="undo"
      labelPosition="right"
      onClick={back}
    />
  </Container>
);

KMeansStepper.propTypes = propTypes;
KMeansStepper.defaultProps = defaultProps;

export default KMeansStepper;
