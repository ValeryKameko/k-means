export default class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  static distance(from, to) {
    return Math.sqrt((from.x - to.x) ** 2 + (from.y - to.y) ** 2);
  }

  static subtract(a, b) {
    return new Point(a.x - b.x, a.y - b.y);
  }

  static sum(a, b) {
    return new Point(a.x + b.x, a.y + b.y);
  }

  static muliply(point, value) {
    return new Point(point.x * value, point.y * value);
  }

  static divide(point, value) {
    return new Point(point.x / value, point.y / value);
  }

  divide(value) {
    return Point.divide(this, value);
  }

  add(point) {
    return Point.sum(this, point);
  }

  subtract(point) {
    return Point.subtract(this, point);
  }

  multiply(value) {
    return Point.multiply(this, value);
  }
}
