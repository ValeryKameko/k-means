import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import { Delaunay } from 'd3-delaunay';

import Group from '../../lib/Group';

const defaultProps = {
  width: 500,
  height: 500,
  radius: 1,
  margin: {
    left: 20, right: 10, top: 10, bottom: 20,
  },
  groups: [],
  padding: 50,
};

const propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  radius: PropTypes.number,
  margin: PropTypes.shape({
    left: PropTypes.number,
    right: PropTypes.number,
    top: PropTypes.number,
    bottom: PropTypes.number,
  }),
  groups: PropTypes.arrayOf(PropTypes.instanceOf(Group)),
  padding: PropTypes.number,
};

const KMeansVisualizer = ({
  width,
  height,
  radius,
  margin,
  groups,
  padding,
}) => {
  const dataPointsRef = useRef();
  const xAxisRef = useRef();
  const yAxisRef = useRef();
  const voronoiCentroidsRef = useRef();
  const voronoiOverlayRef = useRef();

  useEffect(() => {
    const dataPoints = d3.select(dataPointsRef.current);
    const voronoiCentroids = d3.select(voronoiCentroidsRef.current);
    const voronoiOverlay = d3.select(voronoiOverlayRef.current);
    const xAxis = d3.select(xAxisRef.current);
    const yAxis = d3.select(yAxisRef.current);

    const allPoints = groups.flatMap((group) => [group.getCentroid(), ...group.getPoints()]);
    const allX = allPoints.map((point) => point.getX());
    const allY = allPoints.map((point) => point.getY());

    const yScale = d3.scaleLinear()
      .domain([Math.min(...allY) - padding, Math.max(...allY) + padding])
      .range([height, 0]);

    const xScale = d3.scaleLinear()
      .domain([Math.min(...allX) - padding, Math.max(...allX) + padding])
      .range([0, width]);

    const points = groups.flatMap(
      (group, groupIndex) => group.getPoints().map(
        (point) => ({ point, groupIndex }),
      ),
    );
    dataPoints.selectAll('.dot')
      .remove();
    dataPoints.selectAll('.dot')
      .data(points)
      .enter().append('circle')
      .attr('r', radius)
      .attr('cx', (entry) => xScale(entry.point.getX()))
      .attr('cy', (entry) => yScale(entry.point.getY()))
      .style('fill', (entry) => `hsl(${(entry.groupIndex / groups.length) * 360},100%,50%)`)
      .attr('class', 'dot');

    const centroids = groups.map(
      (group, groupIndex) => ({ point: group.getCentroid(), centroidIndex: groupIndex }),
    );
    voronoiCentroids.selectAll('.centroid')
      .remove();
    voronoiCentroids.selectAll('.centroid')
      .data(centroids)
      .enter().append('g')
      .attr('class', 'centroid')
      .attr('transform', (centroid) => `translate(${xScale(centroid.point.getX())}, ${yScale(centroid.point.getY())}) rotate(45)`)
      .append('path')
      .attr('d', d3.symbol().type(d3.symbolCross).size(200))
      .attr('class', 'centroid');

    const delaunay = Delaunay.from(centroids,
      (centroid) => xScale(centroid.point.getX()),
      (centroid) => yScale(centroid.point.getY()));
    const voronoi = delaunay.voronoi([0, 0, width, height]);

    voronoiOverlay.selectAll('path')
      .attr('d', voronoi.render())
      .attr('stroke', '#000000');

    const xAxisGenerator = d3.axisBottom(xScale)
      .ticks(Math.max(5, Math.ceil(width / 20)));
    const yAxisGenerator = d3.axisLeft(yScale)
      .ticks(Math.max(5, Math.ceil(height / 20)));

    xAxis.call(xAxisGenerator);
    yAxis.call(yAxisGenerator);
  }, [groups]);

  return (
    <svg
      width={width + margin.left + margin.right}
      height={height + margin.top + margin.bottom}
    >
      <g transform={`translate(${margin.left}, ${margin.top})`}>
        <g ref={dataPointsRef} />
        <g ref={voronoiCentroidsRef} />
        <g ref={voronoiOverlayRef}>
          <path />
        </g>
      </g>
      <g
        ref={xAxisRef}
        transform={`translate(${margin.left}, ${height + margin.top})`}
        className="axis"
      />
      <g
        ref={yAxisRef}
        transform={`translate(${margin.left}, ${margin.top})`}
        className="axis"
      />
    </svg>
  );
};

KMeansVisualizer.propTypes = propTypes;
KMeansVisualizer.defaultProps = defaultProps;

export default KMeansVisualizer;
