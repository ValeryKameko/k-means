import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { PropTypes } from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import { LabelInputField, SelectField } from 'react-semantic-redux-form';
import { connect } from 'react-redux';

import { SamplerType } from '../../lib/DataGenerator';
import { generatorTypeSelector } from '../selectors/KMeansGeneratorFormSelectors';

const validateNumber = (value, { min = Number.MIN_VALUE, max = Number.MAX_VALUE }) => {
  const number = Number(value);
  if (!value) {
    return 'Required';
  }
  if (Number.isNaN(number)) {
    return 'Must be a number';
  }
  if (number > max || number < min) {
    return `Must be ${min}...${max}`;
  }
  return undefined;
};

const validate = (values) => {
  const errors = {};

  errors.clusters = validateNumber(values.clusters, 1, 100);
  errors.points = validateNumber(values.points, 1, 10000);

  errors.gaussianCounts = validateNumber(values.gaussianCounts, 1, 100);
  errors.gaussianSigma = validateNumber(values.points, 1, 10000);

  return errors;
};

const defaultProps = {
  generatorType: SamplerType.UNIFORM,
  meta: {},
};

const propTypes = {
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  generatorType: PropTypes.number,
  meta: PropTypes.shape({
    error: PropTypes.shape({
      gaussianCounts: PropTypes.string,
      gaussianSigma: PropTypes.string,
      clusters: PropTypes.string,
      points: PropTypes.string,
    }),
  }),
};

const generatorTypes = [
  {
    key: 'Gaussian',
    text: 'Gaussian',
    value: SamplerType.GAUSSIAN,
  },
  {
    key: 'Uniform',
    text: 'Uniform',
    value: SamplerType.UNIFORM,
  },
];

const KMeansGeneratorForm = ({
  submitting,
  handleSubmit,
  generatorType,
  meta: { error },
}) => (
  <Form onSubmit={handleSubmit}>
    <Field
      name="clusters"
      component={LabelInputField}
      label="Clusters"
      error={!!(error && error.clusters)}
    />

    <Field
      name="points"
      component={LabelInputField}
      label="Points"
      error={!!(error && error.points)}
    />

    <Field
      name="generatorType"
      component={SelectField}
      label="Type"
      options={generatorTypes}
    />

    {generatorType === SamplerType.GAUSSIAN
      && (
      <Segment>
        <Field
          name="gaussianCounts"
          component={LabelInputField}
          label="Gaussian counts"
          error={!!(error && error.gaussianCounts)}
        />
        <Field
          name="gaussianSigma"
          component={LabelInputField}
          label="Gaussian sigma"
          error={!!(error && error.gaussianSigma)}
        />
      </Segment>
      )}

    <Form.Field
      control={Button}
      icon="right arrow"
      type="submit"
      labelPosition="right"
      content="Generate"
      disabled={submitting || !!error}
    />
  </Form>
);

KMeansGeneratorForm.propTypes = propTypes;
KMeansGeneratorForm.defaultProps = defaultProps;

const mapStateToProps = (state) => ({
  generatorType: generatorTypeSelector(state),
});

export default connect(mapStateToProps)(
  reduxForm({
    form: 'generatorForm',
    validate,
  })(KMeansGeneratorForm),
);
