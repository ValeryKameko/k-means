import React from 'react';
import PropTypes from 'prop-types';
import {
  Container, Header,
} from 'semantic-ui-react';
import KMeansGeneratorForm from './KMeansGeneratorForm';

const defaultProps = {
};

const propTypes = {
  generate: PropTypes.func.isRequired,
};

const KMeansGenerator = ({ generate }) => (
  <Container fluid>
    <Header as="h2">Generate</Header>
    <KMeansGeneratorForm handleSubmit={generate} />
  </Container>
);

KMeansGenerator.propTypes = propTypes;
KMeansGenerator.defaultProps = defaultProps;

export default KMeansGenerator;
