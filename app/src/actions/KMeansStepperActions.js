import * as actionTypes from '../constants/KMeansStepperConstants';
import { getIsFinished } from '../selectors';

const TIMER_UPDATE_INTERVAL = 100;

let timer = null;

export function pause() {
  return (dispatch) => {
    clearInterval(timer);
    return dispatch({ type: actionTypes.PAUSE });
  };
}

export function step() {
  return (dispatch, getState) => {
    const state = getState();
    const isFinished = getIsFinished(state);
    if (isFinished) { return dispatch(pause()); }
    return dispatch({ type: actionTypes.STEP });
  };
}

export function start() {
  return (dispatch) => {
    timer = setInterval(() => dispatch(step()), TIMER_UPDATE_INTERVAL);
    return dispatch({ type: actionTypes.START });
  };
}

export function restart() {
  return (dispatch) => {
    dispatch(pause());
    return dispatch({ type: actionTypes.RESTART });
  };
}

export function back() {
  return (dispatch) => {
    dispatch(pause());
    return dispatch({ type: actionTypes.BACK });
  };
}
