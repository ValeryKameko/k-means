import * as actionTypes from '../constants';
import {
  pointsSelector, clustersSelector, generatorTypeSelector,
  gaussianSigmaSelector, gaussianCountsSelector,
} from '../selectors/KMeansGeneratorFormSelectors';

export function generate() {
  return (dispatch, getState) => {
    const state = getState();
    const points = pointsSelector(state);
    const clusters = clustersSelector(state);
    const gaussianCounts = gaussianCountsSelector(state);
    const gaussianSigma = gaussianSigmaSelector(state);
    const generatorType = generatorTypeSelector(state);
    return dispatch({
      type: actionTypes.GENERATE, generatorType, points, clusters, gaussianCounts, gaussianSigma,
    });
  };
}

export default generate;
