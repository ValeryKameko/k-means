import { createSelector } from 'reselect';

export const getStepper = (state) => state.stepper;

export const getIsPlaying = createSelector(getStepper, (stepper) => stepper.isPlaying);
export const getStepCount = createSelector(getStepper, (stepper) => stepper.stepCount);
