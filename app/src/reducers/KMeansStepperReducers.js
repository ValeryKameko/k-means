import * as actionTypes from '../constants';

const initialState = {
  isPlaying: false,
  stepCount: 0,
};

const stepper = (state = initialState, { type }) => {
  switch (type) {
    case actionTypes.STEP:
      return {
        ...state,
        stepCount: state.stepCount + 1,
      };
    case actionTypes.START:
      return { ...state, isPlaying: true };
    case actionTypes.PAUSE:
      return { ...state, isPlaying: false };
    case actionTypes.RESTART:
      return { ...state, stepCount: 0 };
    default:
      return state;
  }
};

export default stepper;
