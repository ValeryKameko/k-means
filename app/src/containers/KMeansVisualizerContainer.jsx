/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect } from 'react-redux';

import KMeansVisualizer from '../components/KMeansVisualizer';
import { getGroups } from '../selectors';

const KMeansVisualizerContainer = (props) => <KMeansVisualizer {...props} />;

const mapStateToProps = (state) => ({
  groups: getGroups(state),
});

export default connect(mapStateToProps)(KMeansVisualizerContainer);
