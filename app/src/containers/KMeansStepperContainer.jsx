/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect } from 'react-redux';

import KMeansStepper from '../components/KMeansStepper';
import {
  start, pause, step, restart, back,
} from '../actions/KMeansStepperActions';
import {
  getIsPlaying, getIsFinished, getStepCount, getGroups,
} from '../selectors';

const KMeansStepperContainer = (props) => <KMeansStepper {...props} />;

const mapStateToProps = (state) => ({
  isPlaying: getIsPlaying(state),
  isFinished: getIsFinished(state),
  stepCount: getStepCount(state),
  groups: getGroups(state),
});

export default connect(mapStateToProps, {
  start,
  pause,
  step,
  restart,
  back,
})(KMeansStepperContainer);
