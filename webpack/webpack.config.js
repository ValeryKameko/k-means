/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
const webpackMerge = require('webpack-merge');
const baseConfig = require('./webpack.config.base.js');

module.exports = ({ env }) => {
  process.env.ENV = env;
  const environmentConfig = require(`./webpack.config.${env}.js`);
  return webpackMerge.smart(baseConfig, environmentConfig);
};
