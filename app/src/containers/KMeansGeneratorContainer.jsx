/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect } from 'react-redux';

import {
  generate,
} from '../actions/KMeansGeneratorActions';
import KMeansGenerator from '../components/KMeansGenerator';

const KMeansGeneratorContainer = (props) => <KMeansGenerator {...props} />;

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {
  generate,
})(KMeansGeneratorContainer);
