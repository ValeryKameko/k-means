const isDev = process.env.ENV === 'development';

module.exports = {
  presets: [
    '@babel/env',
    '@babel/react',
  ],
  plugins: [
    '@babel/plugin-transform-runtime',
    [
      'react-css-modules',
      {
        context: './app',
        ...{generateScopedName: isDev ? '[path]_[name]__[local]' : '[hash:base64]'},
        filetypes: {
          '.sass': {
            syntax: 'postcss-sass',
          },
        },
      },
    ],
  ],
}
