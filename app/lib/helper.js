export default function findMinimum(sequence, getter) {
  return sequence.reduce(
    (previousValue, currentValue) => (
      getter(previousValue) > getter(currentValue) ? currentValue : previousValue),
    sequence[0],
  );
}
