import * as actionTypes from '../constants';
import KMeansProcessor from '../../lib/KMeansProcessor';
import DataGenerator from '../../lib/DataGenerator';
import Group from '../../lib/Group';

const initialState = {
  generator: undefined,
  groups: [],
  isGenerated: false,
  isFinished: false,
};

const generate = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.STEP: {
      const newGroups = KMeansProcessor.process(state.groups);
      const isFinished = KMeansProcessor.isFinished(state.groups, newGroups);
      return {
        ...state,
        groups: KMeansProcessor.process(state.groups),
        isFinished,
      };
    }
    case actionTypes.GENERATE: {
      const {
        clusters, points, generatorType, gaussianCounts, gaussianSigma,
      } = action;

      const generator = new DataGenerator(generatorType, {
        min: -50, max: 50, gaussianCounts, gaussianSigma,
      });

      let generatedGroups = Array.from({ length: clusters }, () => generator.generateGroup(0));
      generatedGroups[0] = generator.generateGroup(points);
      generatedGroups = KMeansProcessor.groupPoints(generatedGroups);

      return {
        ...state,
        groups: generatedGroups,
        isGenerated: true,
        generator,
      };
    }
    case actionTypes.RESTART: {
      const { generator, groups } = state;
      const newCentroids = Array.from(
        { length: groups.length },
        () => generator.generateCentroid(),
      );
      const newGroups = groups.map((group, index) => new Group(newCentroids[index], group.points));
      const proccessedGroups = KMeansProcessor.groupPoints(newGroups);

      return {
        ...state,
        groups: proccessedGroups,
        isFinished: false,
      };
    }
    case actionTypes.BACK:
      return {
        ...state,
        groups: [],
        isGenerated: false,
      };
    default:
      return state;
  }
};

export default generate;
