export default class Group {
  constructor(centroid, points) {
    this.centroid = centroid;
    this.points = points;
  }

  getCentroid() {
    return this.centroid;
  }

  getPoints() {
    return this.points;
  }
}
