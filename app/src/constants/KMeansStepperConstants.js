export const START = 'START';
export const PAUSE = 'PAUSE';
export const STEP = 'STEP';
export const RESTART = 'RESTART';
export const BACK = 'BACK';
