import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import stepper from './KMeansStepperReducers';
import generator from './KMeansGeneratorReducers';


const rootReducer = combineReducers({
  form: formReducer,
  generator,
  stepper,
});

export default rootReducer;
