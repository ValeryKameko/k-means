import { createSelector } from 'reselect';

export const getGenerator = (state) => state.generator;

export const getGroups = createSelector(getGenerator, (generator) => generator.groups);
export const getIsGenerated = createSelector(getGenerator, (generator) => generator.isGenerated);
export const getIsFinished = createSelector(getGenerator, (stepper) => stepper.isFinished);
