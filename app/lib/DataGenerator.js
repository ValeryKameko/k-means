import * as d3 from 'd3';
import { randomInt } from 'd3-random';

import Point from './Point';
import Group from './Group';

export const SamplerType = Object.freeze({
  GAUSSIAN: 1,
  UNIFORM: 2,
});

export default class DataGenerator {
  constructor(sampler, {
    min = -50, max = 50, gaussianCounts, gaussianSigma,
  }) {
    const uniformSampler = d3.randomUniform(min, max);
    this.uniformPointSampler = () => new Point(uniformSampler(), uniformSampler());
    switch (sampler) {
      case SamplerType.GAUSSIAN: {
        const gaussianSamplers = Array.from(
          { length: gaussianCounts },
          () => DataGenerator.gaussianPointSampler(uniformSampler, gaussianSigma),
        );
        const indexSampler = randomInt(0, gaussianCounts);
        this.pointSampler = () => DataGenerator.sampleGaussian(indexSampler, gaussianSamplers);
        break;
      }
      case SamplerType.UNIFORM:
        this.pointSampler = this.uniformPointSampler;
        break;
      default:
        throw new Error('No such sampler type');
    }
  }

  generatePoint() {
    return this.pointSampler();
  }

  generateCentroid() {
    return this.uniformPointSampler();
  }

  generateData(count) {
    return Array.from({ length: count }, () => this.generatePoint());
  }

  generateGroup(count) {
    return new Group(this.generateCentroid(), this.generateData(count));
  }

  static sampleGaussian(indexSampler, samplers) {
    const index = indexSampler();
    return samplers[index]();
  }

  static gaussianPointSampler(uniformSampler, gaussianSigma) {
    const gaussianSamplerX = d3.randomNormal(uniformSampler(), gaussianSigma);
    const gaussianSamplerY = d3.randomNormal(uniformSampler(), gaussianSigma);
    return () => new Point(gaussianSamplerX(), gaussianSamplerY());
  }
}
